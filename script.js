'use strict'
// #1

let age = +prompt("What's your age?")
if ( age === null){
    alert("Age wasn't entered")
}
else if(age > 12){
    alert("You're a kiddo")
}
else if(age > 18){
    alert("You're a teenager")
}
else{
    alert("You're a grown dude")
}

// #2

let month = prompt("У якому місяці ви народилися (маленькими літерами)")

switch (month){
    case null:
        alert("Ви не ввели місяць")
        break;
    case "січень":
    case "березень":
    case "травень":
    case "липень":
    case "серпень":
    case "жовтень":
    case "грудень":
        console.log(31)
        break;
    case "квітень":
    case "червень":
    case "вересень":
    case "листопад":
        console.log(30);
        break;
    case "лютий":
        console.log('28/29');
        break;
    default:
        alert("Я не знаю такого місяця")
}